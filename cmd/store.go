package cmd

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	homedir "github.com/mitchellh/go-homedir"
)

type credentials struct {
	Token    string
	Username string
}

var (

	// HOME Find home directory.
	HOME, _ = homedir.Dir()

	// ConfigDir for global scope
	ConfigDir = filepath.Join(HOME, ".oversight")

	// ConfigFile for global scope
	ConfigFile = filepath.Join(ConfigDir, "config.yaml")

	// EnvFile for global scope
	EnvFile = path.Join(ConfigDir, ".env")

	// Auth credentials for global app scope
	Auth credentials

	// VERBOSE for global debugging options
	VERBOSE = false

	// WRITE for global debugging options
	WRITE = false

	// Envs credentials for global app scope
	Envs map[string]string

	// VERSION for Global app
	VERSION = "0.1"

	// API for Global app
	API = "http://api.oversight.in/v1"

	// HOMEPAGE for Global app
	HOMEPAGE = "http://oversight.in/"

	// Donate Endpoint for Global app
	Donate = "https://patreon.com/oversight"

	warnlog    = log.New(os.Stderr, yellow("[WARN]     "), log.Ltime)
	infolog    = log.New(os.Stderr, blue("[INFO]     "), log.Ltime)
	debuglog   = log.New(os.Stderr, yellow("[DEBUG]    "), log.Ltime)
	errlog     = log.New(os.Stderr, red("[ERROR]    "), log.Ltime)
	authlog    = log.New(os.Stderr, "[AUTH]     ", log.Ltime)
	inputlog   = log.New(os.Stderr, bold("[INPUT]    "), log.Ltime)
	successlog = log.New(os.Stderr, green(bold("[SUCCESS]  ")), log.Ltime)

	// ProjectFields for global app scope
	ProjectFields = []string{
		"user",
		"abstract",
		"title",
		"slug",
		"description",
		"significance",
		"acceptCollaborators",
		"acceptInterns",
		"acceptMentors",
		"announcement",
	}

	// ProgressFields for global app scope
	ProgressFields = []string{
		"completed",
		"planned",
		"report_id",
	}
)

// Verbose for global DEBUG scope
func Verbose(data string) {
	if VERBOSE {
		debuglog.Println(data)
	}
}

type userStructure struct {
	Username    string `json:"username"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Website     string `json:"website"`
	Email       string `json:"email"`
	Bio         string `json:"bio"`
	Affiliation string `json:"affiliation"`
	Designation string `json:"designation"`

	Verified bool `json:"verified"`

	Phone int `json:"phone"`

	Instagram string `json:"instagram"`
	Twitter   string `json:"twitter"`
	Linkedin  string `json:"linkedin"`
}

// Progress structure for global app scope
type Progress struct {
	Completed string `json:"completed"`
	Planned   string `json:"planned"`
	ReportID  string `json:"report_id"`
	Date      string `json:"date"`
}

// Draft structure for global app scope
type Draft struct {
	Slug         string `json:"slug"`
	ProjectID    int    `json:"project_id"`
	Status       string `json:"status"`
	Scope        string `json:"scope"`
	Title        string `json:"title"`
	Abstract     string `json:"abstract"`
	Description  string `json:"description"`
	Significance string `json:"significance"`
	Announcement string `json:"announcement"`
	Affiliation string `json:"affiliation"`

	AcceptInterns       bool `json:"accept_interns"`
	AcceptCollaborators bool `json:"accept_collaborators"`
	AcceptMentors       bool `json:"accept_mentors"`

	LastEditDate string `json:"modified"`
}

func green(data string) string {
	return ("\033[92m" + data + "\033[00m")
}

func red(data string) string {
	return ("\033[91m" + data + "\033[00m")
}

func yellow(data string) string {
	return ("\033[93m" + data + "\033[00m")
}

func blue(data string) string {
	return ("\033[94m" + data + "\033[00m")
}

func bold(data string) string {
	return ("\033[01m" + data + "\033[00m")
}

// WriteArchive - Writes downloaded yaml files
func WriteArchive(location, name, data string) {

	var filename = name + ".yaml"

	location = path.Join(location, filename)

	file, err := os.Create(path.Clean(location))

	if err != nil {
		errlog.Fatal(err)
	}

	defer file.Close()

	file = prepareHeader(file)
	file.WriteString("\nkind: user")
	file.WriteString("\nprofile: http://oversight.in/" + name + "\n")
	file.WriteString(data)

	infolog.Println(green("File " + filename + " has been written at " + location))
}

func prepareHeader(file *os.File) *os.File {

	var header = []string{
		"#---------------------------------------------------------------------------#",
		"\n# Archive prepared by Oversight's Research utility.",
		"\n# Written at " + time.Now().String(),
		"\n#-------------------------------------------------------------------------#",
		"\n\n",
	}

	for line := range header {
		file.WriteString(header[line])
	}

	return file
}
