/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os/exec"
	"runtime"

	"github.com/spf13/cobra"
)

var serviceDesk = "mailto:incoming+oversight-cli-14077303-issue-@incoming.gitlab.com"

// bugCmd represents the bug command
var bugCmd = &cobra.Command{
	Use:   "bug",
	Short: "Report bugs or open issues",
	Long: `
Report bugs in this CLI utility.	
We want you to know that Oversight genuinely appreciates your contribution.
If you need to open NEW_ISSUE for any advancement/enhancement kindly use the same command for the purpose.

Official Link: ` + serviceDesk,
	Run: func(cmd *cobra.Command, args []string) {

		if runtime.GOOS == "windows" {
			fmt.Println("Windows sucks. Kindly email the bug at", serviceDesk)
		} else {
			cmd := exec.Command("xdg-open", serviceDesk)
			cmd.Run()
		}
	},
}

func init() {
	rootCmd.AddCommand(bugCmd)
}
