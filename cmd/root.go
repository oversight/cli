package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"

	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "research",
	Short: "Research is a tool to manage your complete Oversight account from terminal.",
	Long: `
Research is a probably paranoid command line utility to manage your complete Oversight account from terminal.
Allows you to view, publish and delete complete project [meta]data along with all other storage services provided by Oversight 
i.e. Bookmarking of other projects, viewing top 10 mind-blowing research projects hosted at Oversight and managing your published intellectual proprty.
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {

	cobra.OnInitialize(initConfig)
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", ConfigFile, "Specify config file")
	rootCmd.PersistentFlags().BoolVarP(&VERBOSE, "verbose", "v", false, "Print verbose logs")
	rootCmd.PersistentFlags().BoolVarP(&WRITE, "write", "w", false, "Write output to file")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	if cfgFile != ConfigFile {
		ConfigFile = cfgFile
		viper.SetConfigFile(ConfigFile)
	} else {
		viper.AddConfigPath(ConfigDir)
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv() // read in environment variables that match
	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		Verbose("Using config file:" + viper.ConfigFileUsed())
		Auth.Token = viper.GetString("TOKEN")
	}

	err := ensureAuth()
	if err != nil {
		errlog.Fatalln(err)
	}
}

func createFreshConfig() error {

	Verbose("Generating fresh config at" + ConfigFile)

	if _, err := os.Stat(ConfigDir); os.IsNotExist(err) {
		err := os.MkdirAll(filepath.Join(ConfigDir), 0600)
		if err != nil {
			errlog.Fatalln(err)
			return err
		}
	}

	file, err := os.Create(ConfigFile)
	defer file.Close()
	if err != nil {
		errlog.Fatalln(err)
		return err
	}

	if err := os.Chmod(ConfigFile, 0777); err != nil {
		errlog.Fatalln(err)
		return err
	}

	_, err = os.Stat(ConfigFile)
	if err == nil {
		viper.SetConfigFile(ConfigFile)
		Verbose("Fresh configuration written")
	} else if os.IsNotExist(err) {
		errlog.Println(err)
		errlog.Fatalln("Couldn't create a fresh configration.")
	}

	return err
}

func ensureAuth() error {

	if len(Auth.Token) <= 0 {
		if _, err := os.Stat(cfgFile); os.IsNotExist(err) {
			createFreshConfig()
		}

		err := Login()
		if err != nil {
			return err
		}
	}
	Verbose("Auth complete with saved token.")

	var err error
	return err
}

// Login for global auth
func Login() error {

	ENDPOINT := API + "/auth/login"

	USERNAME, PASSWORD, err := getCredentials()

	if err != nil {
		errlog.Println(err)
		return err
	}

	client := &http.Client{}
	form := url.Values{}

	form.Add("username", USERNAME)
	form.Add("password", PASSWORD)

	req, err := http.NewRequest("POST", ENDPOINT, strings.NewReader(form.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(req)
	if err != nil {
		errlog.Println(resp.Status)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errlog.Fatal(err)
		return err
	}

	var raw map[string]interface{}
	err = json.Unmarshal([]byte(string(body)), &raw)
	if err != nil {
		errlog.Fatal(err)
	}

	if resp.StatusCode == 200 {
		authlog.Println(green(Auth.Username + " Authenticated \xE2\x9C\x94"))
		Auth.Token = raw["token"].(string)
	} else {
		authlog.Println(resp.StatusCode)
		authlog.Fatal(red(raw["message"].(string)))
	}

	err = updateConfig()
	if err != nil {
		errlog.Fatalln(err)
	}
	return err
}

func getCredentials() (string, string, error) {

	var err error

	fmt.Print("\033[01m[AUTH]\tUsername: \033[00m")

	if _, err = fmt.Scanln(&Auth.Username); err != nil {
		errlog.Fatalln(err)
	}

	fmt.Print("\033[01m[AUTH]\tPassword (hidden): \033[00m")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))

	if err != nil {
		errlog.Fatal(err)
	}

	fmt.Println()

	return Auth.Username, string(bytePassword), err
}

func updateConfig() error {

	err := ioutil.WriteFile(ConfigFile, []byte("TOKEN: "+Auth.Token), os.FileMode(0777))
	if err != nil {
		errlog.Fatalln(err)
	}
	return err
}

func fetchUsername() (string, error) {
	ENDPOINT := API + "/auth/user"

	client := &http.Client{}

	req, err := http.NewRequest("GET", ENDPOINT, nil)
	req.Header.Set("Authorization", "token "+Auth.Token)
	resp, err := client.Do(req)
	if err != nil {
		errlog.Println(resp.Status)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errlog.Fatal(err)
		return "", err
	}

	var raw map[string]interface{}
	err = json.Unmarshal([]byte(string(body)), &raw)
	if err != nil {
		return "", err
	}
	return raw["username"].(string), err
}
