/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os/exec"
	"runtime"

	"github.com/spf13/cobra"
)

// donateCmd represents the donate command
var donateCmd = &cobra.Command{
	Use:   "donate",
	Short: "Contribute to help this open source utlity",
	Long: `
	Allows you to donate amount in order tto help maintain and develope this utility further.
	Oversight promises to use your donation strictly for professional purposes.
	
	We really acknowledge your contribution and can't thank you enough for contributing to open source tech. :)`,
	Run: func(cmd *cobra.Command, args []string) {
		if runtime.GOOS == "windows" {
			fmt.Println("Windows sucks. Can't open the browser. \nPlease visit the link:", Donate)
		} else {
			cmd := exec.Command("xdg-open", Donate)
			cmd.Run()
		}
	},
}

func init() {
	rootCmd.AddCommand(donateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// donateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// donateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
