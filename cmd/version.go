package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var update bool

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Check running version of Research",
	Long: `
Confirms whether you are running the latest version of the utility or not.
Initiates update in case the running version is not latest.
`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Running Version: ", VERSION)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
	getCmd.PersistentFlags().BoolVar(&update, "update", false, "Update service to latest version")

}
