package cmd

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
)

var (
	projectSlug   string
	filename      string
	writeProgress bool
)

type progressList struct {
	Reports []Progress `json:"reports"`
}

// progressCmd represents the progress command
var progressCmd = &cobra.Command{
	Use:   "progress",
	Short: "Add new progress reports for your project",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {

		projectSlug = args[1]
		var err error

		if len(Auth.Username) == 0 {
			Auth.Username, err = fetchUsername()
			if err != nil {
				errlog.Println(err)
				os.Exit(0)
			}
		}

		ENDPOINT := API + "/projects/" + Auth.Username + "/" + projectSlug + "/progress"

		if strings.ToLower(args[0]) == "add" {
			for index := 2; index <= len(args); index++ {
				fileData, err := readFile(args[index])

				if err != nil {
					errlog.Println(err)
					continue
				}

				parsedData, draft, err := parseFile(fileData, ProgressFields)

				if draft {
					warnlog.Println(bold(args[index]), "is an active draft. Decided not to publish it.")
					continue
				} else if err != nil {
					errlog.Println(err)
					continue
				}

				infolog.Println("Loading metadata from", bold(args[index]), "...")
				err = publish(parsedData, ENDPOINT, "POST")

				if err != nil {
					errlog.Println(err)
				}
				successlog.Println("Progress updated for", HOMEPAGE+"/"+Auth.Username+"/"+projectSlug)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(progressCmd)
}

func fetchProgress(ENDPOINT string) (string, error) {

	var err error
	var result []byte

	resp, err := fetch(ENDPOINT)
	if err != nil {
		errlog.Fatalln(err)
		os.Exit(0)
		return string(result), err
	}
	defer resp.Body.Close()

	var pl progressList
	body, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal([]byte(string(body)), &pl)

	if err != nil {
		errlog.Println(err)
		return string(result), err
	}

	processProject := new(bytes.Buffer)
	json.NewEncoder(processProject).Encode(pl)

	result, err = yaml.JSONToYAML(processProject.Bytes())
	if err != nil {
		errlog.Println(err)
		return string(result), err
	}

	return string(result), err
}
