package cmd

import (
	"fmt"
	"strings"
	"time"
)

func progress() {

	currentTime := time.Now()

	for {
		fmt.Println(int(time.Since(currentTime).Seconds()))
		if int(time.Since(currentTime).Seconds()) > 5 {
			fmt.Println("Done")
			break
		} else {
			fmt.Print(strings.Repeat("#", 1))
			time.Sleep(1 * time.Second)
			continue
		}
	}
	fmt.Println()
}
