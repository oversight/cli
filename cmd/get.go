package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
)

type projectlist struct {
	Projects []responsedata `json:"projects"`
}

type responsedata struct {
	Slug   string `json:"slug"`
	Status string `json:"status"`
}

type bookmarkslist struct {
	Bookmarks []bookmarkdata `json:"bookmarks"`
}

type bookmarkdata struct {
	Slug string `json:"slug"`
}

var (
	payload     string
	username    string
	project     string
	projectData Draft
	user        userStructure
	location    string
	progress    bool
)

var getCmd = &cobra.Command{
	Use:   "get <username>/<projectname>",
	Short: "Fetch user or project details",
	Long: `
	Retrieves the required published data of your bookmarks, a user's profile, or the user's projects.
	
	Usage: research get [bookmarks]
						[username]
						[username/project-slug]
						
	Description:
	
		1. Bookmarks - fetches the slugs of all projects you, as an authenticated user, have on your Oversight account.
		2. Username  - gets the profile details of a user. Example: affiliation, full name, phone, linkedin handle, etc.
		3. Project Slug - retrieves complete published metadata of a user. Should be used in the format <username>/<project-slug>.`,
	Run: func(cmd *cobra.Command, args []string) {

		payload = args[0]

		if len(payload) != 0 {

			if strings.Contains(payload, "/") {
				payloadProcessed := strings.Split(payload, "/")
				if len(payloadProcessed[1]) == 0 {
					errlog.Println("Invalid Format. Please use <username>/<projectname>")
					os.Exit(0)
				}

				username = payloadProcessed[0]
				project = payloadProcessed[1]

				if project == "*" {

					projects, err := getListOfProjects(username)
					if err != nil {
						errlog.Println("Couldn't fetch list of user's projects")
						errlog.Println(err)
						os.Exit(0)
					}

					for index := range projects {
						fmt.Println("[", index+1, "]", projects[index])
					}
					fmt.Println("[", 0, "]", "All projects")

					fmt.Print("\nChoose projects to fetch [1,2...]:")

					var choice string
					if _, err = fmt.Scan(&choice); err != nil {
						errlog.Println(err)
					}
					choices := strings.Split(choice, ",")
					allProjects := false

					for key := range choices {
						if choices[key] == "0" {
							allProjects = true
						}
					}

					if allProjects {
						for index := range projects {
							var data string
							if progress {
								infolog.Println("Fetching progress for", bold("oversight.in/"+username+"/"+projects[index]))
								data, err = fetchProgress(API + "/projects/" + username + "/" + projects[index] + "/progress")
							} else {
								infolog.Println("Fetching", bold("oversight.in/"+username+"/"+projects[index]))
								data, err = fetchProject(username, projects[index])
							}
							if err != nil {
								errlog.Println(err)
							} else {
								if WRITE {
									WriteArchive(location, username, string(data))
								} else {
									fmt.Println(data)
								}
							}
						}
					} else {
						for index := range choices {
							var data string
							projectIndex, _ := strconv.Atoi(choices[index])
							projectIndex--
							if progress {
								infolog.Println("Fetching progress for", bold("oversight.in/"+username+"/"+projects[index]))
								data, err = fetchProgress(API + "/projects/" + username + "/" + projects[index] + "/progress")
							} else {
								infolog.Println("Fetching", bold("oversight.in/"+username+"/"+projects[projectIndex]))
								data, err = fetchProject(username, projects[index])
							}
							if err != nil {
								errlog.Println(err)
							} else {
								if WRITE {
									WriteArchive(location, username, string(data))
								} else {
									fmt.Println(data)
								}
							}
						}
					}

				} else {
					var err error
					var data string
					if progress {
						infolog.Println("Fetching progress for", bold("oversight.in/"+username+"/"+project))
						data, err = fetchProgress(API + "/projects/" + username + "/" + project + "/progress")
					} else {
						infolog.Println("Fetching", bold("oversight.in/"+username+"/"+project))
						data, err = fetchProject(username, project)
					}
					if err != nil {
						errlog.Println(err)
					} else {
						if WRITE {
							WriteArchive(location, username, string(data))
						} else {
							fmt.Println(data)
						}
					}
				}

			} else {

				if payload == "bookmarks" {
					data, err := fetchBookmarks()
					if err != nil {
						errlog.Println(err)
						os.Exit(0)
					} else {
						if WRITE {
							WriteArchive(location, Auth.Username, string(data))
						} else {
							fmt.Println(data)
						}
					}

				} else {
					if progress {
						errlog.Println("Invalid usage: --progress flag cannot be used for fetching user's details")
						os.Exit(0)
					}
					username = payload
					data, err := fetchUser()
					if err != nil {
						errlog.Println(err)
						os.Exit(0)
					} else {
						if WRITE {
							WriteArchive(location, username, string(data))
						} else {
							fmt.Println(data)
						}
					}
				}
			}
		} else {
			errlog.Println("Invalid usage. Check --help")
		}
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	getCmd.PersistentFlags().BoolVar(&progress, "progress", false, "Fetch progress of the project")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func fetch(endpoint string) (*http.Response, error) {

	client := &http.Client{}

	req, err := http.NewRequest("GET", endpoint, nil)

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "token "+Auth.Token)

	if err != nil {
		errlog.Fatal(err)
	}

	resp, err := client.Do(req)

	if err != nil {
		errlog.Fatal(err)
	}

	if resp.StatusCode == 401 {
		errlog.Println("Authorization expired. Please re-authenticate.")
		err := Login()

		if err != nil {
			errlog.Println(err)
			os.Exit(0)
		}
		return fetch(endpoint)
	}

	if resp.StatusCode >= 500 {
		errlog.Println("Server error. Please try again later.")
		os.Exit(0)
	}

	return resp, err
}

func fetchBookmarks() (string, error) {

	var err error
	var result []byte

	if len(Auth.Username) == 0 {
		Auth.Username, err = fetchUsername()
		if err != nil {
			errlog.Println(err)
			os.Exit(0)
		}
	}

	infolog.Println("Fetching bookmarks of", bold(Auth.Username))

	resp, err := fetch(API + "/bookmarks/")
	result, _ = ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		errlog.Fatal(err)
		return string(result), err
	}

	var raw bookmarkslist
	err = json.Unmarshal([]byte(string(result)), &raw)

	if err != nil {
		errlog.Fatalln(err)
		return string(result), err
	}

	processUser := new(bytes.Buffer)
	json.NewEncoder(processUser).Encode(raw)

	result, err = yaml.JSONToYAML(processUser.Bytes())
	if err != nil {
		errlog.Fatalln(err)
		return string(result), err
	}

	return string(result), err
}

func fetchUser() (string, error) {

	var err error
	var result []byte

	infolog.Println("Fetching", bold("oversight.in/"+username))

	resp, err := fetch(API + "/users/" + username)
	if err != nil {
		errlog.Fatalln(err)
		return string(result), err
	}
	defer resp.Body.Close()

	userBody, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal([]byte(string(userBody)), &user)

	if err != nil {
		errlog.Fatal(err)
		return string(result), err
	}

	processUser := new(bytes.Buffer)
	json.NewEncoder(processUser).Encode(user)

	result, err = yaml.JSONToYAML(processUser.Bytes())
	if err != nil {
		errlog.Fatal(err)
		return string(result), err
	}

	return string(result), err
}

func fetchProject(user, project string) (string, error) {

	var data []byte

	resp, err := fetch(API + "/projects/" + user + "/" + project)
	if err != nil {
		errlog.Fatalln(err)
		return string(data), err
	}
	defer resp.Body.Close()

	projectBody, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal([]byte(string(projectBody)), &projectData)

	if err != nil {
		errlog.Fatal(err)
		return string(data), err
	}

	processUser := new(bytes.Buffer)
	json.NewEncoder(processUser).Encode(projectData)

	data, err = yaml.JSONToYAML(processUser.Bytes())
	if err != nil {
		errlog.Fatal(err)
		return string(data), err
	}

	return string(data), err
}

func getListOfProjects(username string) ([]string, error) {

	var result []string

	resp, err := fetch(API + "/projects/" + username)
	if err != nil {
		return result, err
	}

	defer resp.Body.Close()

	var ps projectlist
	decodeJSON := json.NewDecoder(resp.Body)
	err = decodeJSON.Decode(&ps)
	if err != nil {
		return result, err
	}

	for index := range ps.Projects {
		result = append(result, ps.Projects[index].Slug)
	}

	return result, err
}
