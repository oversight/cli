package cmd

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
)

var updateReq bool

// publishCmd represents the publish command
var publishCmd = &cobra.Command{
	Use:   "publish <file1> <file2> ...",
	Short: "Publish new project",
	Long: `
	Publish new projects on your authenticated Oversight account.
	
	Users can enter their project information in yaml config files and use this command to automatically
	read those project details and publish them.
	
	Please make sure to only write details of 1 project per file. 
	Multiple projects cannot be parsed from the same file.`,
	Run: func(cmd *cobra.Command, args []string) {

		for index := range args {
			fileData, err := readFile(args[index])

			if err != nil {
				errlog.Println(err)
				continue
			}

			parsedData, draft, err := parseFile(fileData, ProjectFields)

			if draft {
				warnlog.Println(bold(args[index]), "is an active draft. Decided not to publish it.")
				continue
			}

			if err != nil {
				errlog.Println(err)
				continue
			}

			infolog.Println("Loading metadata from", bold(args[index]), "...")

			if len(Auth.Username) == 0 {
				Verbose("Fetching username from server")
				Auth.Username, err = fetchUsername()
				if err != nil {
					errlog.Println(err)
					os.Exit(0)
				}
			}

			err = publish(parsedData, API+"/projects/"+Auth.Username, "POST")

			if err != nil {
				errlog.Println(err)
			} else {
				successlog.Println("Project published at", HOMEPAGE+Auth.Username+"/"+parsedData["slug"])
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(publishCmd)

	publishCmd.Flags().BoolVarP(&updateReq, "update", "u", false, "Update data in case project exists")
}

func publish(data map[string]string, ENDPOINT string, requestType string) error {

	var err error

	client := &http.Client{}
	form := url.Values{}

	for key := range data {
		form.Add(key, data[key])
	}

	req, err := http.NewRequest(requestType, ENDPOINT, strings.NewReader(form.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Authorization", "token "+Auth.Token)
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode > 201 && resp.StatusCode < 500 {

		if resp.StatusCode == 400 {

			if updateReq {
				infolog.Println("Updating project '" + data["slug"] + "'")
				return publish(data, API+"/projects/"+Auth.Username+"/"+data["slug"], "PUT")
			}
			Verbose(resp.Status)
			return errors.New("Project with that slug already exists")

		} else if resp.StatusCode == 401 {
			errlog.Println("Authorization expired. Please re-authenticate.")
			err := Login()

			if err != nil {
				return err
			}
			return publish(data, API+"/projects/"+Auth.Username, "POST")
		}
		return errors.New(resp.Status)

	} else if resp.StatusCode >= 500 {
		Verbose(resp.Status)
		return errors.New("Server Error")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var raw map[string]interface{}
	err = json.Unmarshal([]byte(string(body)), &raw)
	if err != nil {
		return err
	}

	return err
}

func readFile(filename string) (map[string]interface{}, error) {

	var empty map[string]interface{}

	file, err := os.Open(filename)

	if err != nil {
		return empty, err
	}

	defer file.Close()

	b, _ := ioutil.ReadFile(file.Name())

	j2, err := yaml.YAMLToJSON(b)

	if err != nil {
		return empty, err
	}

	var raw map[string]interface{}

	err = json.Unmarshal(j2, &raw)

	if err != nil {
		return empty, err
	}

	return raw, err
}

func parseFile(raw map[string]interface{}, fields []string) (map[string]string, bool, error) {

	var err error
	var empty map[string]string

	final := make(map[string]string)

	draft := false

	for key := range raw {
		for fieldKey := range fields {
			if strings.ToLower(key) == fields[fieldKey] {
				final[key] = raw[key].(string)
			}
		}
		if strings.ToLower(key) == "draft" && raw[key].(bool) == true {
			draft = true
			return empty, draft, err
		}
	}

	return final, draft, err

}
